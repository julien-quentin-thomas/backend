from aiohttp import web

import asyncio
import json
import os.path
import time

with open('CLIENT_ID.txt') as f:
    CLIENT_ID = f.read()

#==== DATA STORAGE AND PERSISTENCE ====
LOCATIONS = []
POSTS = []

def storeData():
    with open('data.json', 'w') as f:
        f.write(json.dumps({ 'locations': LOCATIONS, 'posts': POSTS }))

if os.path.isfile('data.json'):
    with open('data.json') as f:
        data = json.load(f)
    LOCATIONS = data['locations']
    POSTS = data['posts']
    for location in LOCATIONS:
        if 'ownerGoogleId' in location:
            location['googleId'] = location.pop('ownerGoogleId')


#==== ROUTING ====

routes = web.RouteTableDef()

@routes.get('/')
async def hello(request):
    return web.json_response({'status':'ok'})

#==== ROUTING LOCATIONS ====

@routes.post('/locations/new')
async def locationsNew(request):
    try:
        data = await request.json()
        locationInfos = {
            'id': len(LOCATIONS),
            'name': str(data['name']),
            'desc': str(data['desc']),
            'latitude': float(data['latitude']),
            'longitude': float(data['longitude']),
            'googleId': str(data['googleId']),
        }
        LOCATIONS.append(locationInfos)
        return web.json_response(locationInfos)
    except Exception as e:
        return web.json_response({"error": str(e)})

@routes.post('/locations/update')
async def locationsUpdate(request):
    try:
        return web.json_response({})
        data = await request.json()
        locationModif = {}
        id_ = int(data['id'])
        if id_ >= len(LOCATIONS):
            raise Exception("ERROR: Invalid ID!")
        if data['googleId'] != LOCATIONS[id_]['googleId']:
            raise Exception("ERROR: Invalid owner")
        if 'name'      in data: locationModif['name'] = str(data['name'])
        if 'desc'      in data: locationModif['desc'] = str(data['desc'])
        if 'latitude'  in data: locationModif['latitude'] = float(data['latitude'])
        if 'longitude' in data: locationModif['longitude'] = float(data['longitude'])
        LOCATIONS[id_].update(locationModif)
        return web.json_response(LOCATIONS[id_])
    except Exception as e:
        return web.json_response({"error": str(e)})

@routes.get('/locations')
async def locationsGet(request):
    try:
        args = request.query
        if 'id' in args:
            id_ = int(args['id'])
            if id_ >= len(LOCATIONS):
                raise Exception("ERROR: Invalid ID!")
            else:
                return web.json_response(LOCATIONS[id_])
        elif 'all' in args:
            return web.json_response(LOCATIONS)
        else:
            north = float(args['north'])
            east  = float(args['east'])
            south = float(args['south'])
            west  =float(args['west'])
            locations = [ loc for loc in LOCATIONS
                          if south <= loc['latitude'] <= north and west <= loc['longitude'] <= east ]
            return web.json_response(locations)
    except Exception as e:
        return web.json_response({"error": str(e)})


#==== ROUTING POSTS ====

@routes.post('/posts/new')
async def locationsNew(request):
    try:
        data = await request.json()
        locId = int(data['location'])
        if locId >= len(LOCATIONS):
            raise Exception("ERROR: Invalid ID!")

        if data['googleId'] != LOCATIONS[locId]['googleId']:
            raise Exception("ERROR: Invalid owner")

        postInfos = {
            'id': len(POSTS),
            'location': locId,
            'title': str(data['title']),
            'content': str(data['content']),
            'date': time.time(),
        }
        POSTS.append(postInfos)
        return web.json_response(postInfos)
    except Exception as e:
        return web.json_response({"error": str(e)})

@routes.post('/posts/update')
async def locationsUpdate(request):
    try:
        return web.json_response({})
        data = await request.json()
        postModif = {}
        id_ = int(data['id'])
        if id_ >= len(POSTS):
            raise Exception("ERROR: Invalid ID!")
        if data['googleId'] != LOCATIONS[POSTS[id_]['location']]['googleId']:
            raise Exception('ERROR: Invalid owner')
        if 'title'   in data: postModif['title'] = str(data['title'])
        if 'content' in data: postModif['content'] = str(data['content'])
        
        POSTS[id_].update(postModif)
        return web.json_response(POSTS[id_])
    except Exception as e:
        return web.json_response({"error": str(e)})

@routes.get('/posts')
async def locationsGet(request):
    try:
        args = request.query
        if 'id' in args:
            id_ = int(args['id'])
            if id_ >= len(POSTS):
                raise Exception("ERROR: Invalid ID!")
            else:
                return web.json_response(POSTS[id_])
        else:
            since = float(args.getone('since', 0))
            until = float(args.getone('until', POSTS[-1]['date'] if len(POSTS) > 0 else 0))
            if 'all' in args:
                return web.json_response([ post for post in POSTS if since <= post['date'] <= until ])
            elif 'location' in args:
                locId = int(args['location'])
                posts = [ post for post in POSTS
                          if post['location'] == locId and since <= post['date'] <= until ]
                return web.json_response(posts)
            else:
                north = float(args['north'])
                east  = float(args['east'])
                south = float(args['south'])
                west  = float(args['west'])
                locIds = { loc['id'] for loc in LOCATIONS
                        if south <= loc['latitude'] <= north and west <= loc['longitude'] <= east }
                posts = [ post for post in POSTS
                        if since <= post['date'] <= until and post['location'] in locIds ]
                return web.json_response(posts)
    except Exception as e:
        return web.json_response({"error": str(e)})



#==== START SERVER ====

async def main():
    app = web.Application()
    app.add_routes(routes)

    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, '0.0.0.0', 5000)
    await site.start()
    #web.run_app(app, port=5000)

    try:
        while True:
            storeData()
            await asyncio.sleep(30) # every 30 seconds
    finally:
        storeData()
        await runner.cleanup()

asyncio.get_event_loop().run_until_complete(main())


